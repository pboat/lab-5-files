#include <stdio.h>

int main()
{
   int zip, id, price, numOfBeds, numOfBaths, area;
   int count = 0;
   int max = 0;
   int min = 9999999;
   float avg;
   char address[50]; 

   while(scanf("%d,%d,%[^,],%d,%d,%d,%d ", &zip, &id, address, &price, &numOfBeds, &numOfBaths, &area) != EOF)
   {
      count++; 
      if(price > max)
	max = price;
      if(price < min)
	min = price;
   }
   avg = (min + max)/count; 
   printf("%d %d %f, count: %d\n", min, max, avg, count);
   return 0;
}
