#include <stdio.h>

int main()
{
   int zip, id, price, numOfBeds, numOfBaths, area;
   char address[50]; 

   printf("[\n");

   do
   {
      scanf("%d,%d,%[^,],%d,%d,%d,%d ", &zip, &id, address, &price, &numOfBeds, &numOfBaths, &area);
       if(!feof(stdin))
       {
           printf("{ \"zip\": %d, \"address\": \"%s\", \"price\": %d, \"area\": %d },\n", zip, address, price, area);
        }
        else
        {
            printf("{ \"zip\": %d, \"address\": \"%s\", \"price\": %d, \"area\": %d }\n", zip, address, price, area);
        }
   }while(!feof(stdin));

   printf("]\n");

   return 0;
}
