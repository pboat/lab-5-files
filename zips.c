#include <stdio.h>

int main()
{
   int zip, id, price, numOfBeds, numOfBaths, area;
   char address[50]; 
   char fname[20];

   while(scanf("%d,%d,%[^,],%d,%d,%d,%d ", &zip, &id, address, &price, &numOfBeds, &numOfBaths, &area) != EOF)
   {
      FILE *f; //declare file
      sprintf(fname, "%d.txt", zip);  //print to string 

      f = fopen(fname, "a"); //open file with fname sting as file name and append mode 
       
      fprintf(f, "%d\n", zip); //print zip to file
 
   fclose(f);
   }
 
   return 0;
}
