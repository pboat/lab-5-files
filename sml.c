#include <stdio.h>

int main()
{
   int zip, id, price, numOfBeds, numOfBaths, area;
   //int count = 0;
   //int small, medium, large;
   char address[50]; 
   //declare files 
   FILE *f1, *f2, *f3; 
   // open files
   f1 = fopen("small.txt", "w"); 
   f2 = fopen("medium.txt", "w");
   f3 = fopen("large.txt", "w");
   
   if(f3 == NULL && f2 == NULL && f3 == NULL)
   {
      printf("file opening error");
      return 0;
   }

   while(scanf("%d,%d,%[^,],%d,%d,%d,%d ", &zip, &id, address, &price, &numOfBeds, &numOfBaths, &area) != EOF)
   {
      if(area > 2000)
      {
	fprintf(f3, "%s : %d\n", address, area);
      }
      else if(area < 1000)
      {
	fprintf(f1, "%s : %d\n", address, area);
      }
      else 
      {
	fprintf(f2, "%s : %d\n", address, area);
      }
   }
   
   //close file
   fclose(f1);
   fclose(f2);
   fclose(f3);

   return 0;
}
